// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.32 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.32;sub:START;pass:START;ps:flbk:Legacy Shaders/Diffuse,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:True,rprd:True,enco:True,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:False,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3643,x:32692,y:32712,varname:node_3643,prsc:2|diff-3291-OUT,spec-1304-OUT,gloss-3449-OUT,normal-6784-OUT,emission-5802-OUT,alpha-648-OUT,clip-1554-OUT;n:type:ShaderForge.SFN_Slider,id:3421,x:30921,y:33354,ptovrint:False,ptlb:Transparency,ptin:_Transparency,varname:node_3421,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Tex2d,id:2479,x:31571,y:31408,ptovrint:False,ptlb:Normal Map,ptin:_NormalMap,varname:_Refraction,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False|UVIN-9416-UVOUT;n:type:ShaderForge.SFN_Lerp,id:6784,x:31963,y:31347,varname:node_6784,prsc:2|A-4537-OUT,B-2479-RGB,T-7768-OUT;n:type:ShaderForge.SFN_Vector3,id:4537,x:31568,y:31285,varname:node_4537,prsc:2,v1:0,v2:0,v3:1;n:type:ShaderForge.SFN_TexCoord,id:9416,x:31311,y:31408,varname:node_9416,prsc:2,uv:0;n:type:ShaderForge.SFN_Lerp,id:648,x:31351,y:33147,varname:node_648,prsc:2|A-4313-OUT,B-5984-OUT,T-3421-OUT;n:type:ShaderForge.SFN_Vector1,id:5984,x:31000,y:33192,varname:node_5984,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:4313,x:31000,y:33135,varname:node_4313,prsc:2,v1:1;n:type:ShaderForge.SFN_Slider,id:7768,x:31411,y:31592,ptovrint:False,ptlb:Normal Intensity,ptin:_NormalIntensity,varname:_Refraction_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:5;n:type:ShaderForge.SFN_Tex2d,id:5796,x:31963,y:31748,ptovrint:False,ptlb:Diffuse Map (Spec A),ptin:_DiffuseMapSpecA,varname:node_5796,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Color,id:8761,x:31963,y:31564,ptovrint:False,ptlb:Diffuse Color,ptin:_DiffuseColor,varname:node_8761,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:3291,x:32168,y:31677,varname:node_3291,prsc:2|A-8761-RGB,B-5796-RGB;n:type:ShaderForge.SFN_Slider,id:577,x:31109,y:32925,ptovrint:False,ptlb:Metallic,ptin:_Metallic,varname:node_577,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_Lerp,id:4209,x:31525,y:32885,varname:node_4209,prsc:2|A-1808-OUT,B-8066-OUT,T-577-OUT;n:type:ShaderForge.SFN_Vector1,id:1808,x:31265,y:32796,varname:node_1808,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:8066,x:31266,y:32850,varname:node_8066,prsc:2,v1:2;n:type:ShaderForge.SFN_Multiply,id:7596,x:31746,y:32885,varname:node_7596,prsc:2|A-1398-RGB,B-4209-OUT;n:type:ShaderForge.SFN_Color,id:1398,x:31525,y:32741,ptovrint:False,ptlb:Metallic color,ptin:_Metalliccolor,varname:node_1398,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:8757,x:31957,y:32753,varname:node_8757,prsc:2|A-9013-RGB,B-7596-OUT;n:type:ShaderForge.SFN_Slider,id:132,x:31494,y:33407,ptovrint:False,ptlb:BlendAmount,ptin:_BlendAmount,varname:node_132,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:1,max:1;n:type:ShaderForge.SFN_ConstantClamp,id:2848,x:31813,y:33370,varname:node_2848,prsc:2,min:-1,max:1|IN-132-OUT;n:type:ShaderForge.SFN_Add,id:1554,x:31888,y:33207,varname:node_1554,prsc:2|A-7058-R,B-2848-OUT;n:type:ShaderForge.SFN_Tex2d,id:7058,x:31532,y:33219,ptovrint:False,ptlb:BlendMask,ptin:_BlendMask,varname:node_7058,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Power,id:338,x:32281,y:33462,varname:node_338,prsc:2|VAL-1554-OUT,EXP-69-OUT;n:type:ShaderForge.SFN_If,id:1045,x:32548,y:33465,varname:node_1045,prsc:2|A-338-OUT,B-1433-OUT,GT-7247-OUT,EQ-4461-OUT,LT-4461-OUT;n:type:ShaderForge.SFN_Slider,id:5190,x:31718,y:33640,ptovrint:False,ptlb:EdgeWidth,ptin:_EdgeWidth,varname:node_5190,prsc:2,glob:False,taghide:True,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_Multiply,id:8507,x:32724,y:33588,varname:node_8507,prsc:2|A-1045-OUT,B-8722-RGB;n:type:ShaderForge.SFN_Color,id:8722,x:32531,y:33699,ptovrint:False,ptlb:GlowColor,ptin:_GlowColor,varname:node_8722,prsc:2,glob:False,taghide:True,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Vector1,id:7247,x:32298,y:33644,varname:node_7247,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:4461,x:32338,y:33699,varname:node_4461,prsc:2,v1:1;n:type:ShaderForge.SFN_Vector1,id:69,x:32125,y:33524,varname:node_69,prsc:2,v1:10;n:type:ShaderForge.SFN_Vector1,id:96,x:32073,y:33784,varname:node_96,prsc:2,v1:0.003;n:type:ShaderForge.SFN_Multiply,id:1433,x:32125,y:33629,varname:node_1433,prsc:2|A-5190-OUT,B-96-OUT;n:type:ShaderForge.SFN_Tex2d,id:9013,x:31747,y:32347,ptovrint:False,ptlb:Metallic Map,ptin:_MetallicMap,varname:_DiffuseMapSpecA_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Slider,id:1460,x:31244,y:32248,ptovrint:False,ptlb:Metallic Gloss Intensity,ptin:_MetallicGlossIntensity,varname:_SpecularIntensity_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.6,max:1;n:type:ShaderForge.SFN_Lerp,id:1581,x:31747,y:32154,varname:node_1581,prsc:2|A-7710-OUT,B-9043-OUT,T-1460-OUT;n:type:ShaderForge.SFN_Vector1,id:7710,x:31401,y:32090,varname:node_7710,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:9043,x:31401,y:32162,varname:node_9043,prsc:2,v1:1;n:type:ShaderForge.SFN_Multiply,id:3449,x:32043,y:32175,varname:node_3449,prsc:2|A-9013-A,B-1581-OUT;n:type:ShaderForge.SFN_Slider,id:2898,x:31108,y:32574,ptovrint:False,ptlb:Specular Intensity,ptin:_SpecularIntensity,varname:_SpecularofMetal_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Lerp,id:4692,x:31524,y:32536,varname:node_4692,prsc:2|A-6767-OUT,B-5172-OUT,T-2898-OUT;n:type:ShaderForge.SFN_Vector1,id:6767,x:31265,y:32441,varname:node_6767,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:5172,x:31265,y:32500,varname:node_5172,prsc:2,v1:1;n:type:ShaderForge.SFN_Multiply,id:2669,x:31747,y:32536,varname:node_2669,prsc:2|A-6828-RGB,B-4692-OUT;n:type:ShaderForge.SFN_Color,id:6828,x:31524,y:32392,ptovrint:False,ptlb:Specular Color,ptin:_SpecularColor,varname:_SpecularColorofMetal_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Add,id:1304,x:32176,y:32591,varname:node_1304,prsc:2|A-2669-OUT,B-8757-OUT;n:type:ShaderForge.SFN_Slider,id:8605,x:31959,y:32483,ptovrint:False,ptlb:Emission Intensity,ptin:_EmissionIntensity,varname:node_8605,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:5802,x:32092,y:32320,varname:node_5802,prsc:2|A-9013-RGB,B-8605-OUT;proporder:8761-5796-1460-9013-1398-577-6828-2898-8605-2479-7768-3421-132-7058-5190-8722;pass:END;sub:END;*/

Shader "BGlab/Basic + Transparency + Metallic" {
    Properties {
        _DiffuseColor ("Diffuse Color", Color) = (1,1,1,1)
        _DiffuseMapSpecA ("Diffuse Map (Spec A)", 2D) = "white" {}
        _MetallicGlossIntensity ("Metallic Gloss Intensity", Range(0, 1)) = 0.6
        _MetallicMap ("Metallic Map", 2D) = "white" {}
        _Metalliccolor ("Metallic color", Color) = (1,1,1,1)
        _Metallic ("Metallic", Range(0, 1)) = 0.5
        _SpecularColor ("Specular Color", Color) = (1,1,1,1)
        _SpecularIntensity ("Specular Intensity", Range(0, 1)) = 0
        _EmissionIntensity ("Emission Intensity", Range(0, 1)) = 0
        _NormalMap ("Normal Map", 2D) = "black" {}
        _NormalIntensity ("Normal Intensity", Range(0, 5)) = 1
        _Transparency ("Transparency", Range(0, 1)) = 0
        _BlendAmount ("BlendAmount", Range(-1, 1)) = 1
        _BlendMask ("BlendMask", 2D) = "white" {}
        [HideInInspector]_EdgeWidth ("EdgeWidth", Range(0, 1)) = 0.5
        [HideInInspector]_GlowColor ("GlowColor", Color) = (0,1,1,1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal d3d11_9x n3ds wiiu 
            #pragma target 3.0
            uniform float _Transparency;
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform float _NormalIntensity;
            uniform sampler2D _DiffuseMapSpecA; uniform float4 _DiffuseMapSpecA_ST;
            uniform float4 _DiffuseColor;
            uniform float _Metallic;
            uniform float4 _Metalliccolor;
            uniform float _BlendAmount;
            uniform sampler2D _BlendMask; uniform float4 _BlendMask_ST;
            uniform sampler2D _MetallicMap; uniform float4 _MetallicMap_ST;
            uniform float _MetallicGlossIntensity;
            uniform float _SpecularIntensity;
            uniform float4 _SpecularColor;
            uniform float _EmissionIntensity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                UNITY_FOG_COORDS(7)
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD8;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #endif
                #ifdef DYNAMICLIGHTMAP_ON
                    o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
                #endif
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float4 _NormalMap_var = tex2D(_NormalMap,TRANSFORM_TEX(i.uv0, _NormalMap));
                float3 normalLocal = lerp(float3(0,0,1),_NormalMap_var.rgb,_NormalIntensity);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float4 _BlendMask_var = tex2D(_BlendMask,TRANSFORM_TEX(i.uv0, _BlendMask));
                float node_1554 = (_BlendMask_var.r+clamp(_BlendAmount,-1,1));
                clip(node_1554 - 0.5);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float4 _MetallicMap_var = tex2D(_MetallicMap,TRANSFORM_TEX(i.uv0, _MetallicMap));
                float gloss = (_MetallicMap_var.a*lerp(0.0,1.0,_MetallicGlossIntensity));
                float specPow = exp2( gloss * 10.0+1.0);
/////// GI Data:
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                    d.ambient = 0;
                    d.lightmapUV = i.ambientOrLightmapUV;
                #else
                    d.ambient = i.ambientOrLightmapUV;
                #endif
                #if UNITY_SPECCUBE_BLENDING || UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMin[0] = unity_SpecCube0_BoxMin;
                    d.boxMin[1] = unity_SpecCube1_BoxMin;
                #endif
                #if UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMax[0] = unity_SpecCube0_BoxMax;
                    d.boxMax[1] = unity_SpecCube1_BoxMax;
                    d.probePosition[0] = unity_SpecCube0_ProbePosition;
                    d.probePosition[1] = unity_SpecCube1_ProbePosition;
                #endif
                d.probeHDR[0] = unity_SpecCube0_HDR;
                d.probeHDR[1] = unity_SpecCube1_HDR;
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float3 specularColor = ((_SpecularColor.rgb*lerp(0.0,1.0,_SpecularIntensity))+(_MetallicMap_var.rgb*(_Metalliccolor.rgb*lerp(0.0,2.0,_Metallic))));
                float specularMonochrome = max( max(specularColor.r, specularColor.g), specularColor.b);
                float normTerm = (specPow + 8.0 ) / (8.0 * Pi);
                float3 directSpecular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow)*normTerm*specularColor;
                float3 indirectSpecular = (gi.indirect.specular)*specularColor;
                float3 specular = (directSpecular + indirectSpecular);
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += gi.indirect.diffuse;
                float4 _DiffuseMapSpecA_var = tex2D(_DiffuseMapSpecA,TRANSFORM_TEX(i.uv0, _DiffuseMapSpecA));
                float3 diffuseColor = (_DiffuseColor.rgb*_DiffuseMapSpecA_var.rgb);
                diffuseColor *= 1-specularMonochrome;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float3 emissive = (_MetallicMap_var.rgb*_EmissionIntensity);
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                fixed4 finalRGBA = fixed4(finalColor,lerp(1.0,0.0,_Transparency));
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal d3d11_9x n3ds wiiu 
            #pragma target 3.0
            uniform float _Transparency;
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform float _NormalIntensity;
            uniform sampler2D _DiffuseMapSpecA; uniform float4 _DiffuseMapSpecA_ST;
            uniform float4 _DiffuseColor;
            uniform float _Metallic;
            uniform float4 _Metalliccolor;
            uniform float _BlendAmount;
            uniform sampler2D _BlendMask; uniform float4 _BlendMask_ST;
            uniform sampler2D _MetallicMap; uniform float4 _MetallicMap_ST;
            uniform float _MetallicGlossIntensity;
            uniform float _SpecularIntensity;
            uniform float4 _SpecularColor;
            uniform float _EmissionIntensity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float4 _NormalMap_var = tex2D(_NormalMap,TRANSFORM_TEX(i.uv0, _NormalMap));
                float3 normalLocal = lerp(float3(0,0,1),_NormalMap_var.rgb,_NormalIntensity);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float4 _BlendMask_var = tex2D(_BlendMask,TRANSFORM_TEX(i.uv0, _BlendMask));
                float node_1554 = (_BlendMask_var.r+clamp(_BlendAmount,-1,1));
                clip(node_1554 - 0.5);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float4 _MetallicMap_var = tex2D(_MetallicMap,TRANSFORM_TEX(i.uv0, _MetallicMap));
                float gloss = (_MetallicMap_var.a*lerp(0.0,1.0,_MetallicGlossIntensity));
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float3 specularColor = ((_SpecularColor.rgb*lerp(0.0,1.0,_SpecularIntensity))+(_MetallicMap_var.rgb*(_Metalliccolor.rgb*lerp(0.0,2.0,_Metallic))));
                float specularMonochrome = max( max(specularColor.r, specularColor.g), specularColor.b);
                float normTerm = (specPow + 8.0 ) / (8.0 * Pi);
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*normTerm*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float4 _DiffuseMapSpecA_var = tex2D(_DiffuseMapSpecA,TRANSFORM_TEX(i.uv0, _DiffuseMapSpecA));
                float3 diffuseColor = (_DiffuseColor.rgb*_DiffuseMapSpecA_var.rgb);
                diffuseColor *= 1-specularMonochrome;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor * lerp(1.0,0.0,_Transparency),0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal d3d11_9x n3ds wiiu 
            #pragma target 3.0
            uniform float _BlendAmount;
            uniform sampler2D _BlendMask; uniform float4 _BlendMask_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float2 uv1 : TEXCOORD2;
                float2 uv2 : TEXCOORD3;
                float4 posWorld : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float4 _BlendMask_var = tex2D(_BlendMask,TRANSFORM_TEX(i.uv0, _BlendMask));
                float node_1554 = (_BlendMask_var.r+clamp(_BlendAmount,-1,1));
                clip(node_1554 - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal d3d11_9x n3ds wiiu 
            #pragma target 3.0
            uniform sampler2D _DiffuseMapSpecA; uniform float4 _DiffuseMapSpecA_ST;
            uniform float4 _DiffuseColor;
            uniform float _Metallic;
            uniform float4 _Metalliccolor;
            uniform sampler2D _MetallicMap; uniform float4 _MetallicMap_ST;
            uniform float _MetallicGlossIntensity;
            uniform float _SpecularIntensity;
            uniform float4 _SpecularColor;
            uniform float _EmissionIntensity;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i) : SV_Target {
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                float4 _MetallicMap_var = tex2D(_MetallicMap,TRANSFORM_TEX(i.uv0, _MetallicMap));
                o.Emission = (_MetallicMap_var.rgb*_EmissionIntensity);
                
                float4 _DiffuseMapSpecA_var = tex2D(_DiffuseMapSpecA,TRANSFORM_TEX(i.uv0, _DiffuseMapSpecA));
                float3 diffColor = (_DiffuseColor.rgb*_DiffuseMapSpecA_var.rgb);
                float3 specColor = ((_SpecularColor.rgb*lerp(0.0,1.0,_SpecularIntensity))+(_MetallicMap_var.rgb*(_Metalliccolor.rgb*lerp(0.0,2.0,_Metallic))));
                float roughness = 1.0 - (_MetallicMap_var.a*lerp(0.0,1.0,_MetallicGlossIntensity));
                o.Albedo = diffColor + specColor * roughness * roughness * 0.5;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Legacy Shaders/Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
